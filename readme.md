# FAQ Teralab Workspaces

## Table des matières

<!-- TOC -->
* [FAQ Teralab Workspaces](#faq-teralab-workspaces)
  * [Table des matières](#table-des-matières)
  * [Qu'est un Workspace ?](#quest-un-workspace-)
  * [Comment se connecter à une des machines virtuelles du workspace ?](#comment-se-connecter-à-une-des-machines-virtuelles-du-workspace-)
    * [Génération d'une clé SSH](#génération-dune-clé-ssh)
      * [Sous Unix (Ubuntu, macOs, Debian etc.)](#sous-unix-ubuntu-macos-debian-etc)
      * [Sous Windows](#sous-windows)
        * [Avec Microsoft PowerShell](#avec-microsoft-powershell)
        * [Avec PuTTYgen](#avec-puttygen)
    * [Conversion d'une clé SSH non compatible Openssh](#conversion-dune-clé-ssh-non-compatible-openssh)
      * [Clé ssh2 sous Linux](#clé-ssh2-sous-linux)
      * [Clé ppk sous Windows](#clé-ppk-sous-windows)
    * [Connexion à la VM avec sa clé privée SSH](#connexion-à-la-vm-avec-sa-clé-privée-ssh)
      * [Sous Unix (Ubuntu, macOS, Debian etc.)](#sous-unix-ubuntu-macos-debian-etc-1)
      * [Sous Windows](#sous-windows-1)
        * [Avec Microsoft PowerShell](#avec-microsoft-powershell-1)
        * [Avec PuTTY](#avec-putty)
    * [Connexion via une machine de rebond](#connexion-via-une-machine-de-rebond)
      * [Méthodes non permises par défaut](#méthodes-non-permises-par-défaut)
  * [Comment sauvegarder des données ?](#comment-sauvegarder-des-données-)
  * [Comment utiliser un nom de domaine ?](#comment-utiliser-un-nom-de-domaine-)
  * [Comment utiliser un reverse proxy ou un certificat SSL ?](#comment-utiliser-un-reverse-proxy-ou-un-certificat-ssl-)
  * [Comment ouvrir un port TCP ou UDP ?](#comment-ouvrir-un-port-tcp-ou-udp-)
  * [Comment forwarder un port ?](#comment-forwarder-un-port-)
    * [1. Le port forwarding par SSH (local forwarding)](#1-le-port-forwarding-par-ssh-local-forwarding)
    * [2. Un VPN OpenVPN](#2-un-vpn-openvpn)
    * [3. Autres port forwarding via ssh](#3-autres-port-forwarding-via-ssh)
  * [Comment transférer des données ?](#comment-transférer-des-données-)
    * [Exemple utilisation de rsync](#exemple-utilisation-de-rsync)
    * [Network File System](#network-file-system)
  * [Comment utiliser le serveur de mail ?](#comment-utiliser-le-serveur-de-mail-)
  * [Comment bannir des adresses IP à l'origine de tentatives d'accès frauduleuses ?](#comment-bannir-des-adresses-ip-à-lorigine-de-tentatives-daccès-frauduleuses-)
  * [Comment superviser une page web avec un healthcheck ?](#comment-superviser-une-page-web-avec-un-healthcheck-)
  * [Règles d'utilisation](#règles-dutilisation)
    * [Limites de prestation](#limites-de-prestation)
    * [Règles d'adressage IP](#règles-dadressage-ip)
    * [Règles d'utilisation des ports](#règles-dutilisation-des-ports)
    * [Règles sur le système Unix](#règles-sur-le-système-unix)
    * [Recommandations](#recommandations)
  * [Contact](#contact)
<!-- TOC -->

## Qu'est un Workspace ?

Les workspaces de Teralab sont des environnements virtualisés de type Unix au sein desquels nos clients déploient leurs applications.

Ces environnements permettent à nos utilisateurs de se concentrer sur leurs applications, en nous laissant gérer la sécurisation des accès réseau et les tâches liées au système. Nos ingénieurs vous aident également à analyser les performances de votre infrastructure et vous conseillent sur les évolutions possibles.

Les workspaces incluent des fonctionnalités hébergées et managées par l’équipe Teralab. Selon les configurations choisies, on peut trouver :

* Des machines virtuelles Debian ou Ubuntu
* Un mécanisme de sauvegarde / stockage froid
* Du stockage fichier
* Un reverse proxy
* Un pare feu
* Un VPN (en option)
* Un DNS et la gestion de certificats SSL
  * Fourniture d'un nom de domaine Teralab ou utilisation d'un nom de domaine personnalisé
* Un serveur email
* Un bastion SSH (machine de rebond SSH ; option pour les espaces à la sécurité renforcée)
* Un réseau IP privé
* Une supervision de l’environnement. Notamment via un agent Zabbix installé sur chaque VM.
  * Supervision des métriques
  * Healthchecks (ex : check d'une page HTTP) à la demande
* Une maintenance de l’environnement
* Un support au déploiement et à l’exploitation Les configurations matérielles sont faites sur mesure (machines GPU, nombre de vCPU, RAM, disques…). Et l’intégralité des systèmes est hébergée en France.

Des environnements spécifiques sont également proposés à la demande, et constitués de services de cloud public français (stockage objet (type S3), du stockage bloc, Kubernetes managé, serverless, serveurs dédiés, instances de VM GPU dernière génération, load balancers… ).

## Comment se connecter à une des machines virtuelles du workspace ?

La connexion via le protocole **ssh** est le mode d'accès que nous mettons en place par défaut. Elle permet un accès à une invite de commandes Unix, avec les droits **"root"** sur la machine virtuelle, depuis votre ordinateur via internet.

La connexion par **clé publique/privée** est la méthode à utiliser (la méthode par login/mot de passe est proscrite).

L'ordinateur de l'utilisateur doit avoir internet, et une connexion dont certains ports TCP sont ouverts en sortie : les "ephemeral ports" aussi appelés "dynamic ports". Il s'agit des ports TCP **49152 à 65535** en sortie. Par défaut, les pare-feux de Linux, Windows et des box internet laissent passer ces ports en sortie.

Dans un premier temps, l'utilisateur génère une paire de clés (clé publique + clé privée) associées à un mot de passe (passphrase) (s'il n'en a pas déjà une qu'il souhaite utiliser) **au format compatible OpenSSH**. Puis il envoie sa clé publique (pas sa clé privée) à l'équipe Teralab.

> La clé privée doit être gardée en sécurité.

Nous déployons ensuite la clé publique au sein de vos VM :

* Ajout de l'utilisateur aux sudoers (en fonction du rôle de l'utilisateur)
* Ajout de la clé publique sur les VM dans le répertoire : `/.ssh/authorized_keys`

Et nous vous fournirons :

* L'URL de la VM
* Le numéro de port TCP à utiliser pour ssh
* L'utilisateur Unix à utiliser
* Prochainement : l'URL d'un portail permettant de consulter ces informations et d'initier certaines opérations d'administration

### Génération d'une clé SSH

#### Sous Unix (Ubuntu, macOs, Debian etc.)

Dans un terminal, la commande `ssh-keygen` vous générera une paire de clés. 
Nous vous recommandons d'utiliser :
* Un mot de passe (la commande vous le proposera)
* Une clé de 2048 bits minimum

Par défaut, la paire de clés est générée dans le répertoire `~/.ssh` de l'ordinateur.

> Infos pratiques : https://doc.ubuntu-fr.org/ssh


#### Sous Windows

##### Avec Microsoft PowerShell

Avec Powershell (les versions récentes), il faut suivre la même procédure que sous Linux. Par défaut, la paire de clés est générée dans le répertoire `C:\Users\YOURUSER/.ssh/` de l'ordinateur.

> Il est possible que les clés ne soient visible qu'avec PowerShell lancé en mode administrateur.

![alt text](powershell.jpeg)

> Infos pratiques : https://learn.microsoft.com/fr-fr/windows-server/administration/openssh/openssh\_keymanagement#user-key-generation

##### Avec PuTTYgen

Le Key Generator de Putty : l'application *"PuTTYgen"* peut être une alternative à PowerShell. Cependant, elle est plus laborieuse.

Ses paramètres par défaut génèrent un format compatible OpenSSH. Cependant, une difficulté est présente :

* Il faut faire un copier-coller de la clé publique depuis la fenêtre de PuttyGen. Et la sauvegarder à la main dans un fichier.
* Et surtout, **ne pas nous envoyer le fichier qui est exporté quand on clique sur "Save public key"**. Car ce dernier est une clé au format SSH2 et ce n'est pas compatible OpenSSH.
* Quand on clique sur "Save private key", la clé privée est exportée au format .pkk Ce dernier n'est pas compatible OpenSSH.
* Si on clique sur le menu conversion, puis "export openssh key", cela sauvegardera la clé privée au bon format.

### Conversion d'une clé SSH non compatible Openssh

#### Clé ssh2 sous Linux

Si toutefois vous possédez déjà une clé ssh au format ssh2 (non compatible Openssh), et que vous souhaitez utiliser la même clé, il est possible de l'exporter vers un format openssh : 
```shell
ssh-keygen -i -f myuser\_rsa4096 >> myuser\_new
```

Pour vérifier que la clé a été correctement exportée :  
```shell
ssh-keygen -lf myuser\_new
```

#### Clé ppk sous Windows

Si toutefois vous avez déjà une clé au format ppk (non compatible Openssh) que vous souhaitez utiliser, il est possible de la convertir avec Putty : https://www.simplified.guide/putty/convert-ppk-to-ssh-key

![alt text](Putty%20gen.png)




### Connexion à la VM avec sa clé privée SSH

#### Sous Unix (Ubuntu, macOS, Debian etc.)

Dans un terminal, la commande suivante peut être utilisée :

```shell
ssh - p PORTNUMBER -i /THE/PATH/TO/YOUR/KEY nomuser@urldelaVM
```

Par exemple si votre clé **privée** est dans `~/.ssh/` , et qu'elle s'appelle `id_ed25519`, et que l'équipe teralab vous a indiqué :

* 2230 comme port tcp à utiliser pour ssh
* aeinstein comme user Unix à utiliser
* ws05.tl.teralab-datascience.fr comme nom de la VM La commande ssh est alors :

```shell
ssh -p 2230 -i ~/.ssh/id_ed25519 aeinstein@ws05.tl.teralab-datascience.fr
```

Pour plus de simplicité, nous vous recommandons de configurer votre poste (par ex votre PC portable) comme ceci :

1. Éditer ce fichier

```shell
sudo vi .ssh/config
```

2. Y ajouter les lignes

```
Host vm-workspace-teralab
    HostName ws05.tl.teralab-datascience.fr
    User aeinstein
    IdentityFile ~/.ssh/id_ed25519
    Port 2280
```

NB : en théorie une IP publique pourrait être utilisée à la place du nom, mais comme il nous arrive de devoir modifier les IP publiques dans notre DNS. Cela ferait alors échouer la connexion.

3. Cette commande ssh plus courte peut alors être utilisée :

```shell
ssh vm-workspace-teralab
```

#### Sous Windows

##### Avec Microsoft PowerShell

La procédure est la même que sous Unix.

> À la différence que la simplicité offerte par le fichier *.ssh/config* de Linux ne semble pas native sous Windows : https://superuser.com/questions/1537763/location-of-openssh-configuration-file-on-windows

> Dans le cas où l'on souhaite utiliser une Yubikey en tant que clé privée via sa bibliothèque Windows, voici un exemple :

```shell
ssh -p 2231 -I "C:\Users\jdoe\yubico-piv-tool\bin\libykcs11.dll" jdoe@ws72.tl.teralab-datascience.fr
```

##### Avec PuTTY

PuTTY (ne pas confondre avec son utilitaire PuTTYgen) permet d'établir des connexions SSH et de sauvegarder les configurations des connexions. À l'image du fichier *.ssh/config* de Linux. Une fois maitrisée, son utilisation est sans doute plus efficace que de taper les commandes ssh dans PowerShell (hormis pour la génération de clés OpenSSH).

> Plus d'infos : http://marc.terrier.free.fr/docputty/index.html https://www.chiark.greenend.org.uk/\~sgtatham/putty/

### Connexion via une machine de rebond

Cette notion de machine de rebond est aussi appelée "bastion host", "jump host", ou encore "jump box", "edge node" selon les sources.

C'est une machine virtuelle de passerelle, dont la seule fonction est de gérer l'accès SSH aux VM situées dans un réseau IP privé avec davantage de sécurité. Elle permet de centraliser le point d’entrée des connexions SSH à ce réseau privé. Et de tracer les connexions ssh. Elle est alors la seule VM qui peut se connecter aux VM du réseau privé.

Nous proposons ce mécanisme en option lorsque la sécurité est un enjeu important. Selon le niveau de sécurité, l'espace de travail peut prendre une appellation différente :

* espace de travail *autonome* : pas de rebond ssh. La clé ssh de l'utilisateur permet de se connecter en direct aux VM de l'utilisateur.
* espace de travail *administré* : mise en place d'une machine de rebond. La clé ssh de l'utilisateur permet alors de se connecter uniquement à la machine de rebond. Seule la machine de rebond peut se connecter en direct aux VM de l'utilisateur. La possibilité de faire proxy ssh est proscrite.
* espace de travail *confiné* : pas de ssh du tout.

Dans le cas où ce mécanisme de rebond est mis en place, pour se connecter en ssh sur la VM où seront installées vos applications depuis chez vous (depuis un de votre ordinateur par exemple), il faut passer par la machine de rebond.

NB : dans cet exemple, "bastion.ws05.tl.teralab-datascience.fr" est l'URL de la machine de rebond.

1. Connexion à la machine de rebond

```shell
ssh -p 2230 aeinstein@bastion.ws05.tl.teralab-datascience.fr -i ~/.ssh/id_ed25519
```

2. Connexion à la VM depuis la machine de rebond

**Pour la 2ᵉ étape, ce ne serait pas la même clé que celle de l'utilisateur ? ni le même utilisateur ? Commande ci-dessous à corriger**

```shell
bastionw-ws05:~$ ssh -p 2230 aeinstein@vm1.ws05.tl.teralab-datascience.fr -i ~/.ssh/id_ed25519 
```

3. Passage en root (si nécessaire)

```shell
vm1-ws05:~$ sudo -i
root@vm1-ws05:~#
```

#### Méthodes non permises par défaut

1. Connexion directe depuis l'ordinateur vers la VM en effectuant un rebond

Pour garder la traçabilité des commandes SSH, **par défaut, nous n'autorisons pas** l'utilisation de la méthode (certes plus simple d'utilisation) qui consiste à ajouter quelques arguments (en particulier avec l'option **-J**) en une seule commande ssh qu'à exécuter depuis son ordinateur :

Ex, ceci n'est pas supporté :

```shell
ssh -J aeinstein@bastion.ws05.tl.teralab-datascience.fr:2230 aeinstein@vm1.ws05.tl.teralab-datascience.fr -p 2230 -i ~/.ssh/id_ed25519
```

2. Utilisation de proxy ssh

Pour les mêmes raisons, **par défaut, nous n'autorisons pas** l'ajout des lignes suivantes dans le fichier \~/.ssh/config

```
Host bastionhost
  HostName bastion.ws05.tl.teralab-datascience.fr
  User aeinstein
  IdentityFile ~/.ssh/id_ed25519
  Port 2280
Host vm-teralab
  HostName vm1.ws05.tl.teralab-datascience.fr
  ProxyJump bastionhost
  User aeinstein
  ForwardAgent yes
  Port 2280
```

Ce qui permettrait à cette commande ssh de se connecter à la VM :

```shell
ssh vm-workspace-teralab
```

## Comment sauvegarder des données ?

Teralab assure une sauvegarde automatique des configurations essentielles de la VM avec rsnapshot. 
https://doc.ubuntu-fr.org/rsnapshot

Certains répertoires sont ainsi sauvegardés (/root, /etc, /var, ...), et les répertoires supplémentaires que vous souhaitez sauvegarder sont définis ensemble en début de projet (par exemple */data*).

Les fréquences de conservation sont approximativement :

* 14 derniers jours (1/jour pendant 14 jours)
* 4 dernières semaines (1/semaine pendant 4 semaines)
* 12 derniers mois (1/mois pendant 12 mois)

> Toutes les sauvegardes sont détruites en fin de projet.

Pour que la sauvegarde soit utile : 
* Vous devrez mettre régulièrement (ou de manière permanente) les données à sauvegarder dans les répertoires en question.
* Les données placées dans le répertoire de sauvegarde, doivent être relativement statiques pendant la sauvegarde (pour éviter les erreurs de synchronisation de rsnapshot)

**Attention** : les données binaires comme les base de données sont à exclure. Dans le cas des BDD, il faut générer des "dump" texte d'un état propre de la BDD ou des fichiers binaires. Nous pourrons alors convenir d'une sauvegarde de ces dumps.

## Comment utiliser un nom de domaine ?

Notre offre inclut un sous domaine au sein du domaine tl.teralab-datascience.fr. Les noms que nous vous proposons sont de la forme `wsXX-votrepage.tl.teralab-datascience.fr`

> Où XX est le n° de votre workspace

Par exemple, vous pouvez ainsi exposer ces url :

* `ws98-votrepage.tl.teralab-datascience.fr`
* `ws98-unesecondepageweb.tl.teralab-datascience.fr`
* `ws98-votreprojet.tl.teralab-datascience.fr`

Si vous avez besoin d'une de ces fonctionnalités :

* Utiliser un nom de domaine (ou un sous domaine) que vous avez déjà acheté
  * Nous pouvons configurer notre DNS pour qu'il l'utilise
* Acheter un nom de domaine
  * Nous pouvons le souscrire pour vous chez https://www.gandi.net/fr

## Comment utiliser un reverse proxy ou un certificat SSL ?

Si vous avez besoin d'une de ces fonctionnalités :

* Aiguiller le traffic entre plusieurs serveurs web (qu'ils soient ou non installés chacun sur des machines virtuelles différentes du workspace)
* Obtenir un chiffrement TLS pour exposer vos pages web en HTTPS

Notre offre inclut, si besoin, un reverse proxy au sein de votre workspace Teralab.

Notre équipe l'installe et l'opère pour vous. Nous proposons Caddy (https://caddyserver.com) : un logiciel libre et l'un des serveurs web HTTPS les plus avancés au monde. Semblable à Ngnix ou Apache HTTP server.

Caddy peut ainsi obtenir et renouveler automatiquement des certificats TLS pour les sites web que vous déployez dans le workspace.

Nous pouvons également :

* Déployer pour vous un certificat que vous nous fournissez (que vous avez déjà acheté)
* Acheter pour vous un certificat (en général, on l'achète le certificat si vous souhaitez que l'on achète également pour vous le nom de domaine, car il s'agit du même fournisseur : Gandi)

## Comment ouvrir un port TCP ou UDP ?

De manière générale, à part pour quelques services (ex : HTTP ou HTTPS), les ports sont fermés en sortie et en entrée. Il n'y a pas de problèmes à les ouvrir sur demande.

Pour l'ouverture d'un port, vous pouvez nous envoyer un email en précisant :

* Le protocole (TCP / UPD)
* Le(s) port(s)
* Les adresses IP externes autorisées
  * Préciser : en entrée ou en sortie
* Les adresses IP internes (ou le nom de la VM) autorisées
  * Préciser : en entrée ou en sortie
* L'utilisation (ex : *ouverture d'un flux HTTPS pour la VM XXX utilisée pour héberger un serveur web*)

## Comment forwarder un port ?

Dans le cas d'un besoin de redirection de port d'une VM vers un port de son ordinateur, plusieurs options sont possibles :

### 1. Le port forwarding par SSH (local forwarding)

Le forward de port par ssh permet un accès direct d'un ordinateur à une VM d'un workspace tout en gardant l'accès à un autre réseau (par exemple internet) de l'ordinateur sans perturbation ou configuration.

Comme dans les cas précédents, il est possible de la condenser en une seule commande, compatible avec Microsoft Powershell, et compatible avec une machine de rebond SSH. Putty peut également être utilisé pour configurer cette redirection de port.

> Il y a probablement 1 commande à faire par port

Par exemple, on souhaite rediriger le port TCP 50006 de la VM vers le port 8080 de son ordinateur (sans machine de rebond ssh en place)

```shell
ssh -L 8080:localhost:50006 aeinstein@vm1.ws05.tl.teralab-datascience.fr -p2230 -i ~/.ssh/id_ed25519
```

On peut alors garder le terminal ouvert (pour que le tunnel ssh reste ouvert) et taper l'URL `localhost:8080` dans son navigateur internet.

Il est également possible de configurer cela dans le fichier \~/.ssh/config pour plus de facilité :

```
Host vm-teralab
  HostName vm1.ws05.tl.teralab-datascience.fr
  User aeinstein
  LocalForward 127.0.0.1:8080 vm1.ws05.tl.teralab-datascience.fr:50006
  Port 2280
```

Ainsi pour établir le tunnel, il suffit de taper :

```shell
ssh -fN vm-teralab
```

> Avec les options -f : la session SSH s'exécute en arrière-plan -N : désactive la possibilité d'exécuter des commandes sur la VM via ce tunnel ssh (car on ne veut que le port forwarding, pas d'invite de commande)

### 2. Un VPN OpenVPN

C'est la solution adaptée lorsque de nombreux ports sont à ouvrir. Il s'agit d'une configuration plus complexe à mettre en place. Cela peut perturber le fonctionnement de vos autres réseaux et nécessiter une adaptation :

* Ouverture de ports OpenVPN sur votre pare-feu, (car le port de sortie standard vers OpenVPN peut-être bloqué sur votre réseau interne d'entreprise).
* Règles réseau à configurer.
* Installation d'un client OpenVPN.

### 3. Autres port forwarding via ssh

D'autres solutions existent (Remote port forwarding, dynamic port forwarding...), elles peuvent être étudiées selon le besoin.

## Comment transférer des données ?

Pour importer ou exporter des données au sein d'un workspace, de multiples possibilités sont envisageables :

* `scp` : pour copier des fichiers via SSH
* `sftp` : pour un transfert de fichiers via SSH à la manière du protocole FTP
  * Cette méthode fonctionne à la manière d'un système de fichiers à distance. Elle permet par exemple de lister les dossiers/fichiers à distance, supprimer les fichiers à distance...
  * Elle n'est pas ouverte par défaut, mais une simple demande à notre équipe suffit.
* `rsync` : pour une synchronisation unidirectionnelle des répertoires via SSH.
  * Cette méthode a l'avantage de réaliser un transfert incrémental/différentiel : seuls les fichiers ayant changé depuis la dernière synchronisation sont transférés.
  * https://doc.ubuntu-fr.org/rsync
* `sshfs`: Pour monter sur son système de fichier local (de son ordinateur), le système de fichier distant (d'une VM du workspace), à travers une connexion SSH,
  * Cette méthode a l'avantage de conserver les droits utilisateurs, et de manipuler les données distantes avec un gestionnaire de fichier, qu'il soit graphique (Dolphin ...) ou en ligne de commande.
  * https://doc.ubuntu-fr.org/sshfs

Un terminal linux permet d'utiliser ces outils, et des outils plus graphiques existent également si besoin (sur Linux, Mac, ou Windows...).

Dans la plupart des cas, `scp` et `rsync` seront être les méthodes adaptées les plus simples.

### Exemple utilisation de rsync

```shell
rsync -Pav -e "ssh -p 2230 -i /THE/PATH/TO/YOUR/SSH/PRIVATE/KEY" text.txt monidentifiant@ws89-monprojet.tl.teralab-datascience.fr:/data
```

>Cette commande permet de :
>* Préserver les dates, permissions, … des fichiers
>* Parcourir tous les sous-dossiers
>* Afficher le progrès du transfert
>* Utiliser la clé privée SSH correspondant à son utilisateur
>* Utiliser le port 2230 (où se trouve le service SSH dans cet exemple)
>* Synchroniser vers le dossier /data de la VM "monprojet" du workspace "ws89"

### Network File System

L'utilisation du protocole `nfs` pour transférer des données peut être nécessaire pour certains outils, et présenter des performances accrues par rapport à SSHFS.

Par exemple : Mettre en cache les fichiers pour améliorer la vitesse d'accès, verrouiller les fichiers lorsque plusieurs ordinateurs tentent d'écrire simultanément sur le même fichier... 

NFS est déployable à la demande, mais présente une complexité d'utilisation/installation qui n'est pas inclus dans notre offre standard. 
Nous restreignons son utilisation en interne du workspace seulement (ex : entre des VM du workspace, mais pas entre une VM du workspace et un ordinateur à distance via internet)

## Comment utiliser le serveur de mail ?

Le workspace inclut par défaut un accès au serveur d'emails : mail.tl.teralab-datascience.fr Ce dernier peut relayer des mails en SMTP en provenance du domaine tl.teralab-datascience.fr. Ce service de mails s'appuie sur le logiciel Libre Postfix.

Il est par exemple utilisable pour des mails d'alertes en provenance de la machine, et est configuré en standard dans le fichier `/etc/ssmtp/ssmtp.conf`.

Cette configuration est forcée par notre automatisation (effectuée par Ansible très régulièrement). L'automatisation vérifie que la configuration d'origine est toujours bien déployée et que les machines sont opérationnelles. Ce fichier ne doit donc pas être changé.


## Comment bannir des adresses IP à l'origine de tentatives d'accès frauduleuses ?

Des tentatives d'accès frauduleuses peuvent arriver sur divers services exposés sur internet. Par exemple :
* Sur la page d'administration d'un site Wordpress
* Sur le service SSH, et cela même si des précautions ont été prises pour en protéger l'accès (ex : machine de rebond ssh, changement du n° du port pour ssh par un autre que le port 22...),

Si une telle attaque est constatée, nous proposons le déploiement de `fail2ban`. Cette application bannira des adresses IP à l'origine de l'attaque, pendant 3600 secondes et après 3 tentatives.

L'installation est simple :

```shell
apt install fail2ban
```

Et il n'est pas nécessaire de faire une configuration "adhoc", les paramètres par défaut fonctionneront. La configuration système mis en place par notre équipe fait le nécessaire (en particulier pour l'envoi d'emails par `fail2ban`).

## Comment superviser une page web avec un healthcheck ?

Le workspace est supervisé par notre équipe avec Zabbix. Cet outil de supervision remonte les paramètres standards de l'agent Zabbix. Cela couvre beaucoup de paramètres "vitaux" de l'OS et du hardware sous-jacent.

Nous pouvons mettre en place une alerte supplémentaire pour détecter le bon accès à l'une de vos pages web. Ainsi dans le cas où l'application que vous installez dans le workspace serait une application web, nous serions avertis au cas où la page serait inaccessible.

## Règles d'utilisation

### Limites de prestation

Par défaut, vous restez maître des opérations suivantes :
* Sauf demande contraire, vous avez un accès administrateur (sudoer Unix) sur vos VM utilisateurs
* L'installation de vos applications (ex : une base de données, votre programme C++, un serveur Git, une version spécifique de Python ...)
* Les mises à jour des applications que vous avez installées
* La génération de vos clés SSH au format OpenSSH
* La supervision de vos logs applicatifs (ex : les logs de votre application Go)
* La sauvegarde des données vers les dossiers sauvegardés par Teralab (cf. paragraphes sur les sauvegardes)

### Règles d'adressage IP

Pour les utilisateurs expérimentés, il est possible d'utiliser cette plage d'adresses IP en réseau privé à l'intérieur de l'espace de travail : de `172.16.0.0` à `172.31.255.255` 


### Règles d'utilisation des ports

Nous recommandons d'utiliser les ports non privilégiés au sein de vos VM utilisateurs / de vos applications.

> Dans les protocoles de transport tels que TCP, UDP et SCTP, les ports 1-1023 sont par défaut des ports privilégiés, plutôt réservés au système.

### Règles sur le système Unix

Une vérification automatique de l'intégrité du workspace est réalisée chaque jour. Notre automatisation (effectuée par Ansible) vérifie que la configuration d'origine est toujours bien déployée et que les machines sont opérationnelles.

Ainsi certains paramètres ne doivent pas être modifiés par l'utilisateur (au risque qu'ils soient écrasés par ce mécanisme le lendemain), par exemple :

* Les clés ssh (en partie)
* Les mises à jour du noyau Linux
* Les configurations réseau, par ex :
  * Les adresses IP
  * Le serveur DNS
  * Le serveur NTP
* L'agent Zabbix
* Le nom de machines (hosts) :
  * Il est néanmoins possible d'utiliser des Alias s'il y a besoin de changer un nom d'host. Il suffit alors de nous demander de le faire.

Si des ajustements sont nécessaires, vous pouvez contacter notre équipe qui se chargera d'automatiser le déploiement de ces modifications.

### Recommandations

* Nous vous encourageons à être vigilants si vous utilisez un CDN (content delivery network) ou un mécanisme de CORS (Cross-Origin Resource Sharing).
* Le disque supportant le répertoire /root est en général assez petit, et peut être écrasé par mégarde. Nous vous recommandons de ne pas utiliser le répertoire /root.


## Contact

Cette adresse email est à votre disposition pour les demandes techniques à destination de nos administrateurs système : admin@teralab-d...